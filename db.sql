CREATE SCHEMA `hw79` DEFAULT CHARACTER SET utf8;

USE `hw79`;

CREATE TABLE `categories` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `locations` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `items` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `category_id` INT NOT NULL,
  `location_id` INT NOT NULL,
  `description` TEXT NULL,
  `image` VARCHAR(100) NULL,
  `date` DATE NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `category_id_fk_idx` (`category_id` ASC),
  INDEX `location_id_fk_idx` (`location_id` ASC),
  CONSTRAINT `category_id_fk`
    FOREIGN KEY (`category_id`)
    REFERENCES `categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `location_id_fk`
    FOREIGN KEY (`location_id`)
    REFERENCES `locations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE);

INSERT INTO `categories` (`name`, `description`)
VALUES
  ('Мебель', NULL),
  ('Компьютерное оборудование', NULL),
  ('Бытовая техника', NULL);

INSERT INTO `locations` (`name`, `description`)
VALUES
  ('Кабинет #55', NULL),
  ('Кабинет #77', NULL);

INSERT INTO `items` (`name`, `category_id`, `location_id`, `description`, `image`, `date`)
VALUES
  ('Кресло компьютерное KK-345', 1, 1, NULL, NULL, '2019-03-28'),
  ('Ноутбук HP Probook 450', 2, 1, NULL, NULL, '2019-03-28');