const express = require('express');

const router = express.Router();

const createRouter = connection => {
  router.get('/', (req, res) => {
    connection.query('SELECT * FROM `categories`', (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'});
      } else {
        const response = results.map(category => ({
          id: category.id,
          name: category.name
        }));

        res.send(response);
      }
    });
  });

  router.get('/:id', (req, res) => {
    connection.query('SELECT * FROM `categories` WHERE `id` = ?', req.params.id, (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'});
      } else {
        if (results[0]) {
          res.send(results[0]);
        } else {
          res.status(404).send({error: 'Category not found'});
        }
      }
    });
  });

  router.delete('/:id', (req, res) => {
    connection.query('SELECT * FROM `items` WHERE `category_id` = ?', req.params.id, (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'});
      } else {
        if (results && Object.keys(results).length > 0) {
          res.status(409).send({error: 'Category cannot be deleted because it is used'})
        } else {
          connection.query('DELETE FROM `categories` WHERE `id` = ?', req.params.id, (error, results) => {
            if (error) {
              res.status(500).send({error: 'Database error'});
            } else {
              if (results.affectedRows) {
                res.send({message: 'Category successfully removed from database'});
              } else {
                res.status(404).send({error: 'Category not found'});
              }
            }
          });
        }
      }
    })
  });

  router.post('/', (req, res) => {
    const category = req.body;

    if (!category.name) {
      res.status(400).send({error: 'The request does not contain all the necessary data'});
    } else {
      connection.query('INSERT INTO `categories` (`name`, `description`) VALUES (?, ?)',
        [category.name, category.description],
        (error, results) => {
          if (error) {
            res.status(500).send({error: 'Database error'});
          } else {
            res.send({
              id: results.insertId,
              ...category
            });
          }
        }
      );
    }
  });

  router.put('/:id', (req, res) => {
    const data = req.body;

    const queryKeysString = Object.keys(data).map(key => '`' + key + '` = ?').join(', ');
    const queryValuesArray = Object.keys(data).map(key => data[key]);

    connection.query('UPDATE `categories` SET ' + queryKeysString + ' WHERE `id` = ?', [...queryValuesArray, req.params.id], (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'});
      } else {
        if (results.affectedRows) {
          connection.query('SELECT * FROM `categories` WHERE `id` = ?', req.params.id, (error, results) => {
            if (results[0]) {
              res.send(results[0]);
            } else {
              res.status(404).send({error: 'Category not found'});
            }
          });
        } else {
          res.status(404).send({error: 'Category not found'});
        }
      }
    });
  });

  return router;
};

module.exports = createRouter;