const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = connection => {
  const router = express.Router();

  router.get('/', (req, res) => {
    connection.query('SELECT * FROM `items`', (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'});
      } else {
        const response = results.map(item => ({
          id: item.id,
          name: item.name,
          category_id: item.category_id,
          location_id: item.location_id
        }));

        res.send(response);
      }
    });
  });

  router.get('/:id', (req, res) => {
    connection.query('SELECT * FROM `items` WHERE `id` = ?', req.params.id, (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'});
      } else {
        if (results[0]) {
          res.send(results[0]);
        } else {
          res.status(404).send({error: 'Item not found'});
        }
      }
    });
  });

  router.delete('/:id', (req, res) => {
    connection.query('DELETE FROM `items` WHERE `id` = ?', req.params.id, (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'});
      } else {
        if (results.affectedRows) {
          res.send({message: 'Item successfully removed from database'});
        } else {
          res.status(404).send({error: 'Item not found'});
        }
      }
    });
  });

  router.post('/', upload.single('image'), (req, res) => {
    const item = req.body;

    if (req.file) {
      item.image = req.file.filename;
    }

    item.date = new Date().toISOString().slice(0, 10);

    if (!item.category_id || !item.location_id || !item.name) {
      res.status(400).send({error: 'The request does not contain all the necessary data'});
    } else {
      connection.query('INSERT INTO `items` (`category_id`, `location_id`, `name`, `description`, `image`, `date`) VALUES (?, ?, ?, ?, ?, ?)',
        [item.category_id, item.location_id, item.name, item.description, item.image, item.date],
        (error, results) => {
          if (error) {
            res.status(500).send({error: 'Database error'});
          } else {
            res.send({
              id: results.insertId,
              ...item
            });
          }
        }
      );
    }
  });

  router.put('/:id', upload.single('image'), (req, res) => {
    const data = req.body;

    if (req.file) {
      data.image = req.file.filename;
    }

    const queryKeysString = Object.keys(data).map(key => '`' + key + '` = ?').join(', ');
    const queryValuesArray = Object.keys(data).map(key => data[key]);

    connection.query('UPDATE `items` SET ' + queryKeysString + ' WHERE `id` = ?', [...queryValuesArray, req.params.id], (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'});
      } else {
        if (results.affectedRows) {
          connection.query('SELECT * FROM `items` WHERE `id` = ?', req.params.id, (error, results) => {
            if (results[0]) {
              res.send(results[0]);
            } else {
              res.status(404).send({error: 'Item not found'});
            }
          });
        } else {
          res.status(404).send({error: 'Item not found'});
        }
      }
    });
  });

  return router;
};

module.exports = createRouter;