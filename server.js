const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const items = require('./app/items');
const categories = require('./app/categories');
const locations = require('./app/locations');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'user',
  password : '1qaz@WSX29',
  database : 'hw79'
});

app.use('/items', items(connection));
app.use('/categories', categories(connection));
app.use('/locations', locations(connection));

connection.connect((err) => {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }

  console.log('[MySQL] connected as id ' + connection.threadId);

  app.listen(port, () => {
    console.log(`[Server] started on ${port} port`);
  });
});

